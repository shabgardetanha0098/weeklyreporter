﻿namespace WeeklyReporter.Models
{
    public class ProductSale
    {
        public int ProductSaleId { get; set; }
        public int ProductId { get; set; }
        public int[] LastWeeksSalesReport { get; set; }
        public int[] SalesReportForTheLastTwoWeeks { get; set; }
    }
 
}

