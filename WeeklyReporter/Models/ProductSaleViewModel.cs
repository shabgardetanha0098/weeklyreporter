﻿namespace WeeklyReporter.Models
{
    public class ProductSaleViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        public int InCreaseOrDecrease { get; set; }
        public int[] LastWeeksSalesReport { get; set; }
        public int[] SalesReportForTheLastTwoWeeks { get; set; }
    }
}
