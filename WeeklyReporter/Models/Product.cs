﻿namespace WeeklyReporter.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        public int InCreaseOrDecrease { get; set; }


    }
}
