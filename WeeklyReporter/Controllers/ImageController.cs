﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;

namespace WeeklyReporter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        FileExtensionContentTypeProvider fileExtensionContentTypeProvider;

        public ImageController(FileExtensionContentTypeProvider fileExtensionContentTypeProvider)
        {
            this.fileExtensionContentTypeProvider = fileExtensionContentTypeProvider;
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            string pathToFile = Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + id + ".jpg";

            if (!System.IO.File.Exists(pathToFile))
            {
                return NotFound();
            }

            var bytes = System.IO.File.ReadAllBytes(pathToFile);

            if (!fileExtensionContentTypeProvider.TryGetContentType(pathToFile,
                out var contentType))
            {
                contentType = "application/octet-stream";
            }

            return File(bytes, contentType);

        }
    }
}
