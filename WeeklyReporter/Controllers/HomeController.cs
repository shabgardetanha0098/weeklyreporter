﻿using Aspose.Imaging;
using Aspose.Imaging.FileFormats.Jpeg;
using Aspose.Imaging.ImageOptions;
using Aspose.Imaging.Sources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Svg;
using System.ComponentModel.DataAnnotations;
using System.Drawing.Imaging;
using WeeklyReporter.Models;

namespace WeeklyReporter.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        FileExtensionContentTypeProvider fileExtensionContentTypeProvider;

        [MaxLength(int.MaxValue)]
        public string SvgContnent { get; set; }

        public HomeController(ILogger<HomeController> logger, FileExtensionContentTypeProvider fileExtensionContentTypeProvider)
        {
            _logger = logger;
            this.fileExtensionContentTypeProvider = fileExtensionContentTypeProvider;

        }

        public IActionResult Index()
        {
            // Get Data From Api

            Product product = new Product()
            {
                ProductId = 1,
                ProductName = "چاق کباب",
                ProductImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png",
                InCreaseOrDecrease = 3
            };

            ProductSale productSale = new ProductSale()
            {
                ProductId = 1,
                SalesReportForTheLastTwoWeeks = new int[] { 12, 44, 63, 17, 58, 64, 100 },
                LastWeeksSalesReport = new int[] { 2, 4, 6, 7, 28, 65, 10 },
            };

            List<ProductSaleViewModel> viewModels = new List<ProductSaleViewModel>()
            {
                new ProductSaleViewModel()
                {
                    ProductId=1,
                    ProductName = "چاق کباب",
                    ProductImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png",
                    SalesReportForTheLastTwoWeeks = new int[] { 12, 44, 63, 17, 58, 64, 100 },
                    LastWeeksSalesReport = new int[] { 2, 4, 6, 7, 28, 65, 10 },
                    InCreaseOrDecrease=5
                },

                new ProductSaleViewModel()
                {
                    ProductId=1,
                    ProductName = "لقمه زعفرانی",
                    ProductImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png",
                    SalesReportForTheLastTwoWeeks = new int[] { 100,90 , 53, 70, 8, 4, 0 },
                    LastWeeksSalesReport = new int[] { 2, 80, 90, 100, 120, 60, 96 },
                    InCreaseOrDecrease = 1
                },

                new ProductSaleViewModel()
                {
                    ProductId=1,
                    ProductName = "غوراپیچ",
                    ProductImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png",
                    SalesReportForTheLastTwoWeeks = new int[] { 12, 44, 63, 17, 58, 64, 100 },
                    LastWeeksSalesReport = new int[] { 2, 4, 6, 7, 28, 65, 10 },
                    InCreaseOrDecrease = 6
                },

                new ProductSaleViewModel()
                {
                    ProductId=1,
                    ProductName = "چاق kabab",
                    ProductImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png",
                    SalesReportForTheLastTwoWeeks = new int[] { 120, 44, 63, 17, 58, 64, 100 },
                    LastWeeksSalesReport = new int[] { 2, 40, 60, 70, 28, 65, 10 },
                    InCreaseOrDecrease = 10
                },

                new ProductSaleViewModel()
                {
                    ProductId=1,
                    ProductName = "چاق کباب",
                    ProductImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png",
                    SalesReportForTheLastTwoWeeks = new int[] { 12, 44, 63, 17, 58, 64, 100 },
                    LastWeeksSalesReport = new int[] { 2, 4, 6, 7, 28, 65, 10 },
                    InCreaseOrDecrease = 3
                },

                new ProductSaleViewModel()
                {
                    ProductId=1,
                    ProductName = "چاق kabab",
                    ProductImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png",
                    SalesReportForTheLastTwoWeeks = new int[] { 120, 44, 63, 17, 58, 64, 100 },
                    LastWeeksSalesReport = new int[] { 2, 40, 60, 70, 28, 65, 10 },
                    InCreaseOrDecrease = 0
                }
            };
            ViewBag.Guid = Guid.NewGuid().ToString();

            return View(viewModels);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSVG(string[] SvgContnent, string guid)
        {
            int i = 0;
            foreach (var item in SvgContnent)
            {
                i += 1;
                string writeText = item;
                System.IO.File.WriteAllText($"{guid + i.ToString()}.svg", writeText);
            }
            return Redirect($"/Home/CreateImage?guid={guid}");
        }

        public async Task<IActionResult> CreateImage(string guid)
        {
            for (int i = 1; i <= 6; i++)
            {
                var svgDocument = SvgDocument.Open(guid + i + ".svg");
                var bitmap = svgDocument.Draw();
                bitmap.Save(Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + i + ".png", ImageFormat.Png);
                System.IO.File.Delete(guid + i + ".svg");
            }
            return Redirect("/Home/MeregeImages?guid=" + guid);
        }

        public async Task<IActionResult> MeregeImages(string guid)
        {
            // Create a list of images
            string[] imagePaths = { Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + 1 + ".png", Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + 2 + ".png", Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + 3 + ".png", Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + 4 + ".png", Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + 5 + ".png", Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + 6 + ".png" };

            // Get resulting image's size
            List<Size> imageSizes = new List<Size>();
            foreach (string imagePath in imagePaths)
            {
                using (RasterImage image = (RasterImage)Image.Load(imagePath))
                {
                    imageSizes.Add(image.Size);
                }
            }

            int newWidth = imageSizes.Max(size => size.Width);
            int newHeight = imageSizes.Sum(size => size.Height);

            // Combine images into new one
            using (MemoryStream memoryStream = new MemoryStream())
            {
                // Create output source
                StreamSource outputStreamSource = new StreamSource(memoryStream);

                // Create jpeg options
                JpegOptions options = new JpegOptions() { Source = outputStreamSource, Quality = 100 };

                // Create output image
                using (JpegImage newImage = (JpegImage)Image.Create(options, newWidth, newHeight))
                {
                    int stitchedHeight = 0;
                    // Merge images
                    foreach (string imagePath in imagePaths)
                    {
                        using (RasterImage image = (RasterImage)Image.Load(imagePath))
                        {
                            Rectangle bounds = new Rectangle(0, stitchedHeight, image.Width, image.Height);
                            newImage.SaveArgb32Pixels(bounds, image.LoadArgb32Pixels(image.Bounds));
                            stitchedHeight += image.Height;
                        }
                    }
                    for (int i = 1; i <= 6; i++)
                    {
                        System.IO.File.Delete(Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + i + ".png");
                    }

                    // Save the merged image
                    newImage.Save(Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + ".jpg");
                }
            }

            string pathToFile = Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + ".jpg";

            if (!System.IO.File.Exists(pathToFile))
            {
                return NotFound();
            }

            var bytes = System.IO.File.ReadAllBytes(pathToFile);

            if (!fileExtensionContentTypeProvider.TryGetContentType(pathToFile,
                out var contentType))
            {
                contentType = "application/octet-stream";
            }

            System.IO.File.Delete(Directory.GetCurrentDirectory() + "\\wwwroot\\Image\\" + guid + ".jpg");

            return File(bytes, contentType);
        }

    }
}